package com.example.fblogin

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_main2.*
import kotlinx.android.synthetic.main.chattingcardview.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.random.Random

class MainActivity2 : AppCompatActivity() {

    private var auth= FirebaseAuth.getInstance()
    private lateinit var database: DatabaseReference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        supportActionBar?.hide()

        database = Firebase.database.reference
        btn_send.setOnClickListener {
            writeNewUser()
        }

        signout.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            startActivity(Intent(this, MainActivity::class.java))
            finish()
//            firebase.auth().signOut().then(function() {
//                // Sign-out successful.
//                console.log('User Logged Out!');
//            }).catch(function(error) {
//                // An error happened.
//                console.log(error);
//            });
        }




    var query = FirebaseDatabase.getInstance()
        .reference
        .child("users")
        .orderByChild("time")

    val options = FirebaseRecyclerOptions.Builder<Chatting>()
        .setQuery(query,Chatting::class.java)
        .setLifecycleOwner(this)
        .build()


    val adapter = object : FirebaseRecyclerAdapter<Chatting, CategoryHolder>(options) {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryHolder {

            //Log.e("name ",auth.currentUser?.displayName.toString())
            if(viewType == 1)
            {
                Log.e("my side","my side")
                return CategoryHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.chattingcardview, parent, false))
            }
            else
            {

                Log.e("my side","my side")
                return CategoryHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.mychat, parent, false))

            }


        }

        override fun onBindViewHolder(holder: CategoryHolder, position: Int, model: Chatting) {
            Log.d("adapter","hello world")
            holder.bind(model)

        }

//        override fun onDataChanged() {
//
//        }

        override fun getItemViewType(position: Int): Int {
            return if(FirebaseAuth.getInstance().uid == getItem(position).sender)
            {
                0
            }
            else{
                1
            }
            return super.getItemViewType(position)
        }
    }



    chat_rv.layoutManager = LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)
    chat_rv.adapter = adapter

}


class CategoryHolder(val customView: View, var chatting: Chatting? = null) : RecyclerView.ViewHolder(customView) {

    fun bind(chatting: Chatting) {
        with(chatting) {
            customView.message?.text = chatting.message
            customView.tv_name.text = chatting.name
            customView.tv_time.text = chatting.time.subSequence(0,5)
        }
    }
}


fun writeNewUser(){
    val msg=messagetype.text.toString()
    val today=System.currentTimeMillis()
    val time = Calendar.getInstance().time

    val currentTime: String = SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(Date())
    Log.e("time my",currentTime)
    //val hr:Long = TimeUnit.MILLISECONDS.toHours(24).toLong()
    if(msg.isEmpty()){
        Toast.makeText(this,"Type Message", Toast.LENGTH_SHORT).show()
    }
    else {
        val chatting = Chatting(
            name = FirebaseAuth.getInstance().currentUser?.displayName.toString(),
            sender = Firebase.auth.uid.toString(),
            time = currentTime,
            message = messagetype.text.toString(),
            uid = Firebase.auth.uid.toString()
        )
        messagetype.text.clear()
        val db = database.child("users").child(Random.nextInt().toString()).setValue(chatting)
    }
}


}