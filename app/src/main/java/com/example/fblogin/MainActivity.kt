package com.example.fblogin

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.auth.api.identity.BeginSignInRequest
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {


   // private lateinit var signInRequest: BeginSignInRequest
    private lateinit var googleSignInClient : GoogleSignInClient
    private var auth= FirebaseAuth.getInstance()

//    private lateinit var firebaseAuthInstance:Fi

    private val Sign_In_client=123
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        if (auth.uid != null) {
            finish()
            val intent = Intent(this,MainActivity2::class.java)
            val act = auth.currentUser
           // Log.e("")
            intent.putExtra("accountName",act!!.displayName)
           // Log.e("name",acct.displayName.toString())
            startActivity(intent)
            //startActivity(Intent(this, MainActivity2::class.java))
        } else {
            setContentView(R.layout.activity_main)
        }
//        setContentView(R.layout.activity_main)

        // ...
        // Initialize Firebase Auth
        auth = Firebase.auth
        googleLogin()
        bt_signIn.setOnClickListener {
            signIn()
        }
    }

    private fun googleLogin() {

       val signInRequest = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken("763068425499-lrkqt8i0dtadmp4odr6o35uthggnp82j.apps.googleusercontent.com")
            .requestEmail()
            .build()
//        val signInRequest = BeginSignInRequest.builder()
//            .setGoogleIdTokenRequestOptions(
//                BeginSignInRequest.GoogleIdTokenRequestOptions.builder()
//                    .setSupported(true)
//                    // Your server's client ID, not your Android client ID.
//                    .setServerClientId(getString(R.string.your_web_client_id))
//                    // Only show accounts previously used to sign in.
//                    .setFilterByAuthorizedAccounts(true)
//                    .build())
//            .build()
        googleSignInClient= GoogleSignIn.getClient(this,signInRequest)
    }
    private fun signIn()
    {
        val signInIntent = googleSignInClient.signInIntent
        startActivityForResult(signInIntent,Sign_In_client)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Sign_In_client) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account= task.getResult(ApiException::class.java)
                Log.e("account name",account.displayName.toString())
                firebaseAuthWithGoogle(account)
            } catch (e: ApiException) {
                Log.e("firebase", "Google sign in failed", e)
            }
        }
    }

    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        auth.signInWithCredential(credential)
            .addOnSuccessListener(this) { authResult ->

                val intent = Intent(this,MainActivity2::class.java)
                intent.putExtra("accountName",acct.displayName)
                Log.e("name",acct.displayName.toString())
                startActivity(intent)
                //finish()
                Toast.makeText(
                    this, "Authentication Successfully.",
                    Toast.LENGTH_SHORT
                ).show()
                Log.d("firebase","successful")

            }
            .addOnFailureListener(this) { e ->
                Toast.makeText(
                    this, "Authentication failed.",
                    Toast.LENGTH_SHORT
                ).show()
                Log.d("firebase","un - successful")
            }
    }
}